<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRgisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('user')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function register(UserRgisterRequest $request)
    {
        $user = User::create($request->only('name', 'email') + [
            'password' => Hash::make($request->password)
        ]);
        $token = $user->createToken('user')->accessToken;
        return response()->json(['token' => $token], 200);
    }
}
