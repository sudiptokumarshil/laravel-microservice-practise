<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::paginate(10);
        return ProductResource::collection($products);
    }
    public function store()
    {

    }
    public function show($id)
    {
        $product = Product::find($id);
        return new ProductResource($product);
    }
    public function update()
    {

    }
    public function destroy($id)
    {
        
    }
}
